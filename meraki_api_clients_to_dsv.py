# Standard libs
import os
import logging
import datetime
import requests

def get_all_meraki_network_devices():
    """
    Retrieve the devices of each known meraki network.
    """
    for network_id in NETWORK_IDS.values():
        retry = 3
        while retry:
            try:
                get_meraki_network_devices(network_id=network_id)
            except:
                retry -= 1
            else:
                break


def get_meraki_network_devices(network_id):
    """
    Retrieve the devices of a single meraki network.
    """
    json_results = _make_meraki_api_call(request="networks/{0}/devices".format(network_id))
    for device in json_results:
        if not device in KNOWN_MERAKI_DEVICES:
            KNOWN_MERAKI_DEVICES.append(device["serial"])


def get_all_meraki_device_clients():
    """
    Retrieve the clients of all known meraki devices.
    """
    for device_serial in KNOWN_MERAKI_DEVICES:
        retry = 3
        while retry:
            try:
                get_meraki_device_client(device_serial=device_serial)
            except:
                retry -= 1
            else:
                break


def get_meraki_device_client(device_serial):
    """
    Retrieve the clients of a single meraki device.
    """
    json_results = _make_meraki_api_call(request="devices/{0}/clients?timespan=300".format(device_serial))
    for client in json_results:
        KNOWN_MERAKI_CLIENTS.append(client)


def write_known_clients_to_dsv(output_file, delimiter=','):
    """
    Write the details of each client to a delimiter separated file.
    """
    with open(output_file, 'w') as ofile:
        double_line_check = []
        ofile.write('"ip","hostname","mac"\n')
        for client in KNOWN_MERAKI_CLIENTS:
            line = '"{0}"{1}"{2}"{1}"{3}"\n'.format(
                    client["ip"], delimiter, client["dhcpHostname"],
                    client["mac"])
            if not line in double_line_check:
                ofile.write(line)
                double_line_check.append(line)


def _make_meraki_api_call(request):

    headers = {'X-Cisco-Meraki-API-Key': API_KEY}
    return requests.get('https://api.meraki.com/api/v0/{0}'.format(request), headers=headers).json()


def _prepare_output_folder():

    if not os.path.exists(os.path.dirname(OUTPUT_FILE)):
        os.mkdir(os.path.dirname(OUTPUT_FILE))
    if os.path.exists(OUTPUT_FILE):
        os.remove(OUTPUT_FILE)  # Prevent outdated DNS data (i.e. prefer no results over outdated results)
    if os.path.exists(LOG_FILE):
        os.remove(LOG_FILE)




API_KEY = "INSERT_HERE"
OUTPUT_FILE = "/folder/clients_ip_hostname_dsv"
LOG_FILE = "/folder/clients_ip_hostname_dsv.log"
GLOBAL_RETRY = 3

NETWORK_IDS = {"network_1": "N_1234567890"}
KNOWN_MERAKI_DEVICES = []
KNOWN_MERAKI_CLIENTS = []

if __name__ == '__main__':

    _prepare_output_folder()
    logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG)
    logging.log(level=logging.INFO, msg='[{0}] Starting run.'.format(datetime.datetime.now()))
    get_all_meraki_network_devices()
    get_all_meraki_device_clients()
    write_known_clients_to_dsv(output_file=OUTPUT_FILE)

